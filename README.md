In initial testing moving pods from replicaset to replicaset has not resulted
in pods restarting. This is a design goal.  However, creating new deployments 
does not seem to pick up existing pods. It seems k8's finds the existing 
replicaset and then creates a new replicaset for the deployment and scales 
the existing replicaset down to 0 replicas.

The question now becomes do we need deployments?  Since the plan is to do our 
own routing we may be able to create k8's service by just creating endpoints to
existing pods.

A headless service may be the key here instead of deployment and normal service

In this scenario:

1. ReplicaSet controls pods in initial hot pool
2. Pods are moved to another replicaSet when pushed into production
3. A headless service is setup to front the replicaset pods in production


See:  https://kubernetes.io/docs/concepts/services-networking/service/#with-selectors

It looks as if Isio might be able to route to headless services (this is eventual
goal - Need to create a Service Entry)

See:  https://github.com/istio/istio/issues/7495#issuecomment-431626617

https://istio.io/docs/reference/config/istio.networking.v1alpha3/#ServiceEntry


The spec below will change based on findings from above

Goal: basic CRD that wraps a  Replication Controller


fields on the CRD
	labelForCRD
	number of replicas
	nodeSelector
	image
	labelForPods


Testing with Fake Client

	use the fake client to test creating and updating the new CRD


Watches :

    1. the controller will watch replicaSets owned by the custom CRD



How does the CRD behave

	
	1.  will create pods with labels of specific type via replication controller
	2.  pods will follow template defined in the CRD - will transfer values down
	    to CRD create the object



Two parts of controller - the add and the reconcile

1.  in the add we setup a watch to watch replicasets owned by our CRD
2.  in the reconcile we handle replicaset creation and altering based on 
    changes to our CRD



References to use for development:


https://itnext.io/building-an-operator-for-kubernetes-with-kubebuilder-17cbd3f07761

https://itnext.io/building-an-operator-for-kubernetes-with-the-sample-controller-b4204be9ad56

https://itnext.io/testing-kubernetes-go-applications-f1f87502b6ef

https://medium.com/firehydrant-io/stay-informed-with-kubernetes-informers-4fda2a21da9e

https://github.com/kubernetes/client-go/tree/master/examples/create-update-delete-deployment

https://itnext.io/testing-kubernetes-go-applications-f1f87502b6ef

https://itnext.io/testing-kubernetes-go-applications-f1f87502b6ef



Integration with Gitlab:

We should have CI/CD enabled in gitlab - should run unit tests and then integration 
tests agains real k8's cluster 

this is TBD - 


Integration with other Skynetes components

It is envisioned that the **skynetes-kubeapi** project will contain code to imperatively
invoke operations on the CRD via client-go.  

In turn the **skynetes-kubeapi** will expose a GRPC interface that will allow
other components running in the cluster invoke operations involving the CRD.  In particular the pods
in the HotBox will be able to be moved from original replication controller
to new Deployments








